/* Populate database with sample data. */

INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Agumon', 'Feb 3, 2020', 0, true, 10.23);
INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Gabumon', 'Nov 15, 2018',2 ,true, 8);
INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Pikachu', 'Jan 7, 2021',1 ,false, 15.04);
INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Devimon', 'May 12, 2017',5 ,true, 11);

INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Charmander', 'Feb 8, 2020',0 ,false, -11);
INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Plantmon', 'Nov 15, 2021',2 ,true, -5.7);
INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Squirtle', 'Apr 2, 1993',3 ,false, -12.13);
INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Angemon', 'Jun 12, 2005',1 ,true, -45);
INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Boarmon', 'Jun 7, 2005',7 ,true, 20.4);
INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Blossom', 'Oct 13, 1998',3 ,true, 17);
INSERT INTO animals (name, date_of_birth, escape_attempts, neutered, weight_kg)
VALUES ('Ditto', 'May 14, 2022',4 ,true, 22);


INSERT INTO owners(full_name, age) VALUES('Sam Smith', 34);
INSERT INTO owners(full_name, age) VALUES('Jennifer Orwell', 19);
INSERT INTO owners(full_name, age) VALUES('Bob', 45);
INSERT INTO owners(full_name, age) VALUES('Melody Pond', 77);
INSERT INTO owners(full_name, age) VALUES('Dean Winchester', 14);
INSERT INTO owners(full_name, age) VALUES('Jodie Whittaker', 38);

-- Update insert data into the species table
INSERT INTO species(name) Values('Pokemon');
INSERT INTO species(name) Values('Digimon');

-- Modify your inserted animals so it includes the species_id value:
-- Start the transaction for modifying the species_id column in animals table
BEGIN;

-- If the name ends in "mon" it will be Digimon
UPDATE animals SET species_id = 2 WHERE name LIKE '%mon';

-- All other animals are Pokemon
UPDATE animals SET species_id = 1 WHERE name NOT LIKE '%mon';

-- Update commit the transaction to make sure it persists
COMMIT;

-- Modify your inserted animals to include owner information (owner_id)
-- Update start the transaction for adding owner id to animals table
Begin;

-- Update Sam Smith owns Agumon
UPDATE animals SET owner_id = 1 WHERE name LIKE 'Agumon';

-- Update Jennifer Orwell owns Gabumon and Pikachu
-- Gabumon
UPDATE animals SET owner_id = 2 WHERE name LIKE 'Gabumon';

-- Pikachu
UPDATE animals SET owner_id = 2 WHERE name LIKE 'Pikachu';

-- Update Bob owns Devimon and Plantmon
-- Devimon
UPDATE animals SET owner_id = 3 WHERE name LIKE 'Devimon';

-- Plantmon
UPDATE animals SET owner_id = 3 WHERE name LIKE 'Plantmon';

-- Melody Pond owns Charmander, Squirtle, and Blossom.
-- Charmander
UPDATE animals SET owner_id = 4 WHERE name LIKE 'Charmander';

-- Squirtle
UPDATE animals SET owner_id = 4 WHERE name LIKE 'Squirtle';

-- Blossom
UPDATE animals SET owner_id = 4 WHERE name LIKE 'Blossom';

-- Dean Winchester owns Angemon and Boarmon
-- Angemon
UPDATE animals SET owner_id = 5 WHERE name LIKE 'Angemon';

-- Boarmon
UPDATE animals SET owner_id = 5 WHERE name LIKE 'Boarmon';

-- Commited transaction to make sure it persisted
COMMIT;

-- insert data into the vets table
INSERT INTO vets (name, age, date_of_graduation) 
VALUES ('William Tatcher', 45, 'Apr 23, 2000'),
('Maisy Smith', 26, 'Jan 17, 2019'),
('Stephanie Mendez', 64, 'May 4, 1981'),
('Jack Harkness', 38, 'Jun 8, 2008');

-- insert data into the specializations table
INSERT INTO specializations(species_id,vet_id)
SELECT species.id, vets.id FROM species JOIN vets ON species.name = 'Pokemon' AND vets.name = 'William Tatcher';
INSERT INTO specializations(species_id,vet_id)
SELECT species.id, vets.id FROM species JOIN vets ON species.name = 'Digimon' AND vets.name = 'Stephanie Mendez';
INSERT INTO specializations(species_id,vet_id)
SELECT species.id, vets.id FROM species JOIN vets ON species.name = 'Pokemon' AND vets.name = 'Stephanie Mendez';
INSERT INTO specializations(species_id,vet_id)
SELECT species.id, vets.id FROM species JOIN vets ON species.name = 'Digimon' AND vets.name = 'Jack Harkness';

-- insert data into the visits table
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(1,1,'2020-05-24');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(1,3,'2020-07-22');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(2,4,'2021-02-02');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(3,2,'2020-01-05');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(3,2,'2020-03-08');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(3,2,'2020-05-14');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(4,3,'2021-05-04');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(5,4,'2021-02-24');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(6,2,'2019-12-21');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(6,1,'2020-08-10');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(6,2,'2021-04-07');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(7,3,'2019-09-29');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(8,4,'2020-10-03');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(8,4,'2020-11-04');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(9,2,'2019-01-24');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(9,2,'2019-05-15');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(9,2,'2020-02-27');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(9,2,'2020-08-03');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(10,3,'2020-05-24');
INSERT INTO visits(animal_id, vet_id, date_of_visit) VALUES(10,1,'2021-01-11');
